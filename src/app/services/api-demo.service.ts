import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiDemoService {

  private url = environment.HOST+'/api-demo';

  constructor(
    private httpClient: HttpClient
  ) { }

  public getMessage(): Observable<any> {
    return this.httpClient.get<any>(this.url+'/demo')
  }
}
