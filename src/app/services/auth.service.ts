import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CognitoUserPool } from 'amazon-cognito-identity-js';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url = `${environment.URL_API_GATEWAY}`;


  constructor(
    private httpClient: HttpClient,
  ) { }

  //Metodo para registrar un usuario
  signUp(user: string, email: string, pass: string): Observable<any> {
    let params: HttpParams = new HttpParams()
      .set('auth', 'register')
      .set('name', user)
      .set('email', email)
      .set('password', pass)
    return this.httpClient.get(`${this.url}/logeo?${params}`);
  }

  //Metodo para logearse
  signIn(email: string, pass: string): Observable<any> {
    let params: HttpParams = new HttpParams()
      .set('auth', 'login')
      .set('email', email)
      .set('password', pass)
    return this.httpClient.get(`${this.url}/logeo?${params}`);
  }

  //Verificar si el usuario esta logeado en api gateway
  async isAuth(): Promise<boolean> {
    let params: HttpParams = new HttpParams().set('auth', 'validateSession');

    try {
      const res: any = await this.httpClient.get<any>(`${this.url}/logeo`, { params }).toPromise();
      console.log("Este es el rest sesion: ", res.validSession);
      return res.validSession;
    } catch (err) {
      console.error('Error:', err);
      return false;
    }
  }

  //Verificar si el usuario esta logeado en api gateway
  logout(): Observable<any> {
    let params: HttpParams = new HttpParams().set('auth', 'logout');
    return this.httpClient.get<any>(`${this.url}/logeo`, { params });
  }

  //Verificar si el usuario esta logeado
  // isAuth(): boolean {
  //   var isAuth = false;
  //   //Datos de grupo(poll)
  //   var poolData = {
  //     UserPoolId: environment.UserPoolId, // Your user pool id here
  //     ClientId: environment.ClientId, // Your client id here
  //   };
  //   var userPool = new CognitoUserPool(poolData);
  //   var currentUser = userPool.getCurrentUser();
  //   if (currentUser) {
  //     currentUser.getSession((err: any, session: any) => {
  //       if (err) {
  //         alert(err.message || JSON.stringify(err));
  //       }
  //       isAuth = session.isValid();
  //     })
  //   }
  //   return isAuth;
  // }

  getToken(): string {
    //Buscamos el key que tenga el accessToken, y retornamos el token
    for (let i = 0; i < localStorage.length; i++) {
      if (localStorage.key(i).endsWith('accessToken')) {
        return localStorage.getItem(localStorage.key(i));
      }
    }
    return null;
  }




}
