import { Component } from '@angular/core';

@Component({
  selector: 'app-empty-route',
  template: '<h1>NOT FOUND 404</h1>',
})
export class EmptyRouteComponent {}
