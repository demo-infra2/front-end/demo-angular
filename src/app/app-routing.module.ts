import { APP_BASE_HREF } from "@angular/common";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { EmptyRouteComponent } from "./empty-route/empty-route.component";
import { MainComponent } from "./components/main.component";
import { SignInComponent } from "./components/auth/sign-in/sign-in.component";
import { MainModule } from "./components/main.module";
import { AuthModule } from "./components/auth/auth.module";

const routes: Routes = [
  { path: "", pathMatch: "full", redirectTo: "/sign-in" },//Sirve para rutas vacias
  { path: "**", component: EmptyRouteComponent }//Sirve para ruta no existentes

// {
//   path: 'sign-in',
//   component:SignInComponent,
//   // loadChildren: () => import('./components/auth/auth.module').then(x => x.AuthModule),
// },
// {
//   path: 'main',
//   component: MainComponent,
//   loadChildren: () => import('./components/main.module').then(x => x.MainModule)
// }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    MainModule,
    AuthModule
  ],
  exports: [RouterModule],
  providers: [{ provide: APP_BASE_HREF, useValue: "/" }],
})
export class AppRoutingModule {}