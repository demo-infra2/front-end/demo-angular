import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CommonModule } from "@angular/common";
import { MainComponent } from "./main.component";
import { MainRoutingModule } from "./main-routing.module";
import { RouterModule, Routes } from "@angular/router";
import { MenuComponent } from "./menu/menu.component";
import { MaterialModule } from "../shared/material.module";


@NgModule({
    declarations: [
        MainComponent
    ],
    imports: [
        MainRoutingModule,
        CommonModule,
        MaterialModule
    ]
})
export class MainModule { }
