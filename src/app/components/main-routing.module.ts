import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main.component';
import { MenuComponent } from './menu/menu.component';
import { EmptyRouteComponent } from '../empty-route/empty-route.component';
import { MainGuard } from '../guards/main.guard';

const routes: Routes = [
  // { path: '', redirectTo: 'menu', pathMatch: 'full' },
  { path: 'menu', component: MenuComponent,canActivate:[MainGuard] }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
