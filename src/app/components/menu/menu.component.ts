import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationDetails, CognitoUser, CognitoUserPool } from 'amazon-cognito-identity-js';
import { ApiDemoService } from 'src/app/services/api-demo.service';
import { AuthService } from 'src/app/services/auth.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'uni-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  title: string = '';
  constructor(
    private router: Router,
    private apiDemoService: ApiDemoService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.getMessage();
  }

  getMessage() {
    this.apiDemoService.getMessage().subscribe((rest) => {
      this.title = '-' + rest.message;
    }, (err) => {
      console.log("Hubo un error al llamar el metodo ");
    })
  }

  onLogout() {
    this.authService.logout().subscribe(() => {
      localStorage.removeItem('accessToken');
      this.router.navigate(['sign-in']);
    }, (err) => {
      alert(`Hubo un error al cerrar la sesion: ${err}`);
    })
  }

  // onLogout() {
  //   //Datos de grupo(poll)
  //   var poolData = {
  //     UserPoolId: environment.UserPoolId, // Your user pool id here
  //     ClientId: environment.ClientId, // Your client id here
  //   };
  //   var userPool = new CognitoUserPool(poolData);
  //   var currentUser = userPool.getCurrentUser();
  //   currentUser?.signOut();
  //   this.router.navigate(['sign-in']);
  // }

}
