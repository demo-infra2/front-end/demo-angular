import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { AuthRoutingModule } from "./auth-routing.module";
import { SignInComponent } from "./sign-in/sign-in.component";
import { MaterialModule } from "src/app/shared/material.module";
import { SignUpComponent } from './sign-up/sign-up.component';


@NgModule({
    declarations: [
        SignInComponent,
        SignUpComponent
    ],
    imports: [
        AuthRoutingModule,
        RouterModule,
        CommonModule,
        MaterialModule,
    ],
    exports:[MaterialModule]
})
export class AuthModule { }
