import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CognitoUserAttribute, CognitoUserPool } from 'amazon-cognito-identity-js';
import { Iuser } from 'src/app/interfaces/iuser';
import { AuthService } from 'src/app/services/auth.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'uni-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  logForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService:AuthService
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.logForm = this.formBuilder.group({
      email: [''],
      name: [''],
      pass: ['']
    });
  }

  onRegister() {
    this.authService.signUp(this.name.value,this.email.value, this.pass.value).subscribe((result) => {
      alert('Se envio el correro de confirmacion');;
      // this.router.navigate(['/menu']);
    }, (err) => {
      console.log("Cayo en error al logearse: ", err)
      alert(`Hubo un error al registrarse: ${err}`);
    });
  }


  // onRegister(){
     //Datos de grupo(poll)
    //  var poolData = {
    //   // UserPoolId: environment.UserPoolId, // Your user pool id here
    //   // ClientId: environment.ClientId, // Your client id here
    // };
    // var userPool = new CognitoUserPool(poolData);
    // //atributos
    // var attrList=[];
    // var iuser:Iuser={
    //   email: this.email?.value,
    //   given_name: this.givename?.value,
    //   nickname: this.nickname?.value
    // }
    // for(let key in iuser){
    //   var attrData={
    //     Name:key,
    //     Value:iuser[key]
    //   }
    //   var attr=new CognitoUserAttribute(attrData);
    //   attrList.push(attr);
    // }
    // //Sign up
    // userPool.signUp(this.email?.value,this.pass?.value,attrList,[],(err,result)=>{
    //   if(err){
    //     alert(err.message || JSON.stringify(err));  
    //   }
    //   var newUser=result?.user;
    //   console.log(JSON.stringify(newUser));
    //   alert('Se envio el correro de confirmacion');
    // })
  // }

  get email() {
    return this.logForm.get('email');
  }

  get name() {
    return this.logForm.get('name');
  }

  get pass() {
    return this.logForm.get('pass');
  }



}
