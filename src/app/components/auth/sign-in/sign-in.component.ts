import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationDetails, CognitoUser, CognitoUserPool } from 'amazon-cognito-identity-js';
import { AuthService } from 'src/app/services/auth.service';
import { environment } from 'src/environments/environment';
import { assetUrl } from 'src/single-spa/asset-url';

@Component({
  selector: 'uni-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  logForm!: FormGroup;

  imageUrl: string = assetUrl('/img/wallpaperLogin.jpg');



  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.logForm = this.formBuilder.group({
      user: [''],
      pass: ['']
    });
  }

  onLogin() {
    this.authService.signIn(this.user.value, this.pass.value).subscribe((result) => {
      localStorage.setItem('accessToken', result.token);
      this.router.navigate(['/menu']);
    }, (err) => {
      console.log("Cayo en error al logearse: ", err)
      alert(`Hubo un error al logearse: ${err}`);
    });
  }

  // onLogin() {
  //   //Datos de grupo(poll)
  //   var poolData = {
  //     UserPoolId: environment.UserPoolId, // Your user pool id here
  //     ClientId: environment.ClientId, // Your client id here
  //   };
  //   var userPool = new CognitoUserPool(poolData);
  //   //Datos del usuario
  //   var userData={
  //     Username:this.user?.value,
  //     Pool:userPool
  //   }
  //   var congnitoUser=new CognitoUser(userData);
  //   //Credenciales para el registro
  //   var authData={
  //     Username:this.user?.value,
  //     Password:this.pass?.value
  //   }
  //   var authDetails=new AuthenticationDetails(authData);
  //   congnitoUser.authenticateUser(authDetails,{
  //     onSuccess:(result)=>{
  //       console.log("Token: ",result.getAccessToken().getJwtToken())
  //       this.router.navigate(['/menu']);
  //     },
  //     onFailure:(err)=>{
  //       console.log("Cayo en error ");
  //       alert(err.message || JSON.stringify(err));
  //     }
  //   })
  // }

  get user() {
    return this.logForm.get('user');
  }

  get pass() {
    return this.logForm.get('pass');
  }

}
