# Usa una imagen base, por ejemplo, el servidor de desarrollo de Angular
FROM node:16 AS build
WORKDIR /app

# Copia los archivos del microfrontend Angular
COPY package*.json .

RUN npm install

COPY . .

RUN ["npm", "run", "build"]
# Segunda etapa para una imagen más pequeña
FROM nginx:alpine

COPY ./nginx.conf /etc/nginx/nginx.conf

## Remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*

# Copia el resultado de la compilación de Angular desde la etapa anterior
COPY --from=build /app/dist/mf-menu /usr/share/nginx/html

# Expone el puerto en el cual el microfrontend Angular estará disponible
EXPOSE 4200:4200

ENTRYPOINT ["nginx", "-g", "daemon off;"]